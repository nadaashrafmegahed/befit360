package pages;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;

//Profile Picture Screen after Sign Up

public class ProfilePicturePage extends PageBase {

	public ProfilePicturePage(AndroidDriver<MobileElement> driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	@AndroidFindBy(id="com.robusta.befit:id/tv_skip")
	MobileElement Skipbtn;

	//Explicit wait

	//Method to click on skip btn on Profile Picture 
	public void clickonSkip()
	{
		Skipbtn.click();	
	}

}
