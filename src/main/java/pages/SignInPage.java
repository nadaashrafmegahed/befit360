package pages;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;

//All elements and methods in SignIN screen 
public class SignInPage extends PageBase{

	public SignInPage(AndroidDriver<MobileElement> driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	//Explicit wait 

	@AndroidFindBy(id="com.robusta.befit:id/textinput_placeholder")
	public
	MobileElement EMail;

	@AndroidFindBy(id="com.robusta.befit:id/edt_password")
	MobileElement Password;

	@AndroidFindBy(id="com.robusta.befit:id/btn_sign_in")
	MobileElement SignInbtn;

	//Method to send Email
	public void setEmail(String email)
	{
		EMail.sendKeys(email);
	}


	//Method to send Password
	public void setPassword(String password)
	{
		Password.click();
		Password.sendKeys(password);
	}

	//Method to Click on SignIn 
	public void ClickOnSignIn()
	{
		SignInbtn.click();
	}

}

