package pages;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class IntroPage extends PageBase
{

	public IntroPage(AndroidDriver<MobileElement> driver) {
		super(driver);
	}

	@AndroidFindBy(id="com.robusta.befit:id/skip_done_tv")
	public MobileElement SkipButton;

	//method for skip first screen in application
	public void clickSkip()
	{
		SkipButton.click();
	}

}
