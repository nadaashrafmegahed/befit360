package pages;


import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;


public class SignUpWithEmailPage extends PageBase {

	public SignUpWithEmailPage(AndroidDriver<MobileElement> driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}


	//Find elements in SignUp with email screen 
	
	@AndroidFindBy(id="com.robusta.befit:id/textinput_placeholder")
	MobileElement NameField;

	@AndroidFindBy(id="com.robusta.befit:id/email_et")
	MobileElement EmailField;

	@AndroidFindBy(id="com.robusta.befit:id/password_et")
	MobileElement PasswordField;

	@AndroidFindBy(id="com.robusta.befit:id/phone_number_et")
	public
	MobileElement PhoneNumberField;

	@AndroidFindBy(id="com.robusta.befit:id/age_et")
	MobileElement AgeField;

	@AndroidFindBy(id="com.robusta.befit:id/address_et")
	MobileElement AddressField;

	@AndroidFindBy(id="com.robusta.befit:id/male_radio_btn")
	MobileElement MaleRadioButton;

	@AndroidFindBy(id="com.robusta.befit:id/female_radio_btn")
	MobileElement FemaleRadioButton;

	@AndroidFindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.NumberPicker[3]/android.widget.EditText")
	MobileElement Year;

	@AndroidFindBy(id="android:id/button1")
	MobileElement OkBtnOfAgePop;

	@AndroidFindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.NumberPicker[2]/android.widget.EditText")
	MobileElement Day;

	@AndroidFindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.NumberPicker[1]/android.widget.EditText")
	MobileElement Month;
	
	@AndroidFindBy(id="com.robusta.befit:id/sign_up_btn")
	public
	MobileElement SignUpBtnInSignUpWithMail;
	
	@AndroidFindBy(id="com.robusta.befit:id/btn_popup_done")
	MobileElement OkBtnInSuccessPopUp;

	public void setName (String name)

	{
		NameField.sendKeys(name);
	}

	public void setEmail (String email)

	{
		EmailField.sendKeys(email);
	}

	
	public void setPassword (String password)

	{
		PasswordField.click();
		PasswordField.sendKeys(password);
	}

	public void setPhoneNumber (String phone)

	{
		PhoneNumberField.click();
		PhoneNumberField.sendKeys(phone);
	}
	public void setAge ()

	{
		AgeField.click();
	}
	public void clickonSignUp ()

	{
		SignUpBtnInSignUpWithMail.click();
	}
	public void setAddress (String address)

	{   AddressField.click();
		AddressField.sendKeys(address);
	}

	public void selectMale ()

	{
		MaleRadioButton.click();
	}

	public void selectFemale ()

	{
		FemaleRadioButton.click();
	}
	public void selectYear ()

	{
		Year.click();
		Year.clear();

	}

	public void selectDay (String day)

	{
		Day.clear();
		Day.sendKeys(day); 
	}
	public void selectMonth (String month)

	{
		Month.clear();
		Month.sendKeys(month);
	}
	
	public void clickonOK ()

	{
		OkBtnOfAgePop.click();
	}
	
	public void ClickonSuccessbtn ()

	{    
		OkBtnInSuccessPopUp.click();
	}
}
