package pages;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class SignUpOptionsPage extends PageBase {
	public SignUpOptionsPage(AndroidDriver<MobileElement> driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	@AndroidFindBy(id="com.robusta.befit:id/sign_up_btn")
	MobileElement SignUpButton;

	//Method to click on SignUP on SignUp Options Screen to SignUp with email
	public void clickOnSignUp()
	{
		SignUpButton.click();
	}

}
