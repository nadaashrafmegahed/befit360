package tests;

import java.io.IOException;

import org.openqa.selenium.interactions.Keyboard;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import data.ExcelReader;
import pages.IntroPage;
import pages.ProfilePicturePage;
import pages.SignUpOptionsPage;
import pages.SignUpWithEmailPage;
import utilities.Scrolldown;

public class SignUpWithEmailTest extends TestBase {
    String FileName="UserData.xlsx";

	// get data from Excel Reader class 
	@DataProvider(name="ExcelData")
	public Object[][] userRegisterData() throws IOException
	{
		ExcelReader ER = new ExcelReader();
		return ER.getExcelData(FileName);
	}
	
	
	@Test(dataProvider="ExcelData")
	public void signupWithEmail(String name,String email,String password,String phone,String address)
	{
		WebDriverWait wait=new WebDriverWait(driver, 10);
		Scrolldown Scroll =new Scrolldown();
		IntroPage intro =new IntroPage(driver);
		SignUpOptionsPage options =new SignUpOptionsPage(driver);
		SignUpWithEmailPage signup =new SignUpWithEmailPage(driver);
		ProfilePicturePage profile =new ProfilePicturePage(driver);
		//Steps
		wait.until(ExpectedConditions.visibilityOf(intro.SkipButton));
		intro.clickSkip();
		options.clickOnSignUp();
		signup.setName(name);
		signup.setEmail(email);
		signup.setPassword(password);
		driver.hideKeyboard();
		Scroll.scrolldown(signup.PhoneNumberField);
		signup.setPhoneNumber(phone);
		driver.hideKeyboard();
		signup.setAge();
		/*signup.selectYear();
		/*signup.selectDay("14");
		signup.selectMonth("Mar");*/
		signup.clickonOK();
		signup.setAddress(address);
		driver.hideKeyboard();
		signup.selectFemale();
		signup.clickonSignUp();
	    signup.ClickonSuccessbtn();
		profile.clickonSkip();
		
	}
}
