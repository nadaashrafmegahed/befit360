package tests;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import data.ExcelReader;
import pages.SignInPage;

public class SignInTest extends TestBase{
 String FileName="UserData.xlsx";

@DataProvider(name="ExcelData")
public Object[][] userRegisterData() throws IOException
{
	ExcelReader ER = new ExcelReader();
	return ER.getExcelData(FileName);
}


@Test(dataProvider="ExcelData")
public void signin (String name,String email,String password,String phone,String address)
{
	WebDriverWait wait=new WebDriverWait(driver, 10);
	SignInPage login =new SignInPage(driver);
	wait.until(ExpectedConditions.visibilityOf(login.EMail));
	login.setEmail(email);
	login.setPassword(password);
	driver.hideKeyboard();
	login.ClickOnSignIn();
}
}
