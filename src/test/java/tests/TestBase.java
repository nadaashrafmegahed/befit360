package tests;

import java.net.MalformedURLException;
import java.net.URL;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeTest;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class TestBase {
public static AndroidDriver<MobileElement> driver;
 @BeforeTest
 
public void startDriver () throws MalformedURLException
{
	DesiredCapabilities cap= new DesiredCapabilities();
	cap.setCapability("deviceName", "Samsung Galaxy S7 edge");
	cap.setCapability("platformVersion", "8");
	cap.setCapability("platformName", "Android");
	cap.setCapability("appPackage", "com.robusta.befit");
	cap.setCapability("appActivity", "com.robusta.befit.features.splash.SplashActivity");
	//cap.setCapability("unicodeKeyboard", false);
	//cap.setCapability("resetKeyboard", false);

	driver = new AndroidDriver<MobileElement>(new URL("http://localhost:4723/wd/hub"),cap);

	
}


}