package tests;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import pages.IntroPage;

public class IntroTest extends TestBase {
	
	@Test
	//TestCase for skipping intro screen 
	public	void skipIntro ()
	{
		IntroPage intro =new IntroPage(driver);
		

		intro.clickSkip();
	}	
}