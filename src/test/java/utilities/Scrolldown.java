package utilities;

import java.time.Duration;

import org.openqa.selenium.Dimension;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;
import io.appium.java_client.touch.offset.PointOption;
import tests.TestBase;

public class Scrolldown extends TestBase{
	public void scrolldown(MobileElement element)
	{
 Dimension dimension =driver.manage().window().getSize();
 int ScrollStart=(int) (dimension.getHeight() * 0.8);
 int ScrollEnd=(int) (dimension.getHeight() * 0.1);
AndroidTouchAction actions =new AndroidTouchAction(driver).press(PointOption.point(0,ScrollStart)).
		 waitAction(WaitOptions.waitOptions(Duration.ofSeconds(3)))
		 .moveTo(PointOption.point(0,ScrollEnd))
		 .release().
		 perform();
       actions.tap(ElementOption.element(element)).perform();
	}
}
